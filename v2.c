#include <stdio.h>
#include <stdlib.h>
#include <string.h>



typedef struct
{
  char chiffre;
  struct cell *suivant;
} cell;

typedef struct 
{
  int negatif;
  struct cell *chiffres;
  int compt_ptr;
} num;

typedef struct 
{
    struct num *nombre;
    struct operande *suivant;
} operande;


operande* pile_op = NULL; //Pile contenant les operandes lues
num* variables[26]; //Tableau de pointeurs vers des num (0 = a, 26 = z)

//Ajoute une nouvelle cell
void push(cell **p, char a)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  tmp->chiffre = a;
  tmp->suivant = *p;
  *p = tmp;
  //printf("push %i\n", tmp->chiffre - 48);
}

//Ajoute une nouvelle operande 
void push_op(operande **op, num* n) 
{
    operande *tmp = (operande*)malloc(sizeof(operande));
    tmp->nombre = n;
    tmp->suivant = *op;
    *op = tmp;
}

cell * inversePile(cell *p)
{
  //struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  cell *tmp = NULL;
  while(p != NULL)
    {
      //printf("inv p %i\n", **p->chiffre - 48);
      push(&tmp, p->chiffre); 
      //printf("inv tmp %i\n", tmp->chiffre - 48);
      p = p->suivant;
    }
  return tmp;
}

cell *add(cell *p1, cell *p2)
{
  //mod: modulo, r: retour, s: somme
  int mod = 0, r = 0, s = 0;
  //struct cell *res = (struct cell *)malloc(sizeof(struct cell));
  cell *res = NULL;
  while(p1 != NULL && p2 != NULL)
    {
      //ascii code char: 0 = 48, A = 65, a = 97
      s = (p1->chiffre - 48) + (p2->chiffre - 48) + r;
      //printf("s: %i\n", s);
      mod = s % 10;
      //printf("mod: %i\n", mod);
      r = s / 10;
      //printf("r: %i\n", r);
      //push(&res, mod+48);
      push(&res, mod+48);

      p1 = p1->suivant;
      p2 = p2->suivant;
    }
  
  if(p1 == NULL)
    {
      while(p2 != NULL)
	{
	  s = (p2->chiffre - 48) + r;
	  mod = s % 10;
	  r = s / 10;
	  //push(&res, mod+48);
	  push(&res, mod+48);
	  p2 = p2->suivant;
	}
    } else
    {
      while(p1 != NULL)
	{
	  s = (p1->chiffre - 48) + r;
	  mod = s % 10;
	  r = s / 10;
	  //push(&res, mod+48);
	  push(&res, mod+48);
	  p1 = p1->suivant;
	}
    }
  if(r != 0)
    push(&res, r+48);
  return  inversePile(res);
}

cell *mult(cell *p1, cell *p2)
{
  //mul:  multiplication, r: retour, mod: modulo
  int mul = 0, r = 0 , mod = 0;
  cell * tmp1 = NULL;
  cell * tmp2 = NULL;
  //struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  cell *tmp = NULL;
 
  while(p2 != NULL)  
    {
      while(p1 != NULL)
	{
	  mul = (p2->chiffre - 48) * (p1->chiffre - 48) + r;
	  mod = mul % 10;
	  r = mul / 10;
	  //printf("mult mul %i\n", mul);
	  //printf("mult mod %i\n", mod);
	  //printf("mult r %i\n", r);
	  push(&tmp, mod+48);
	  //printf("mult tmp %i\n", tmp->chiffre - 48);
	  p1 = p1->suivant;
	}
      if (r != 0)
	push(&tmp, r+48);
      //tmp1 = 
      p2 = p2->suivant;
    }
  return inversePile(tmp);
}

void printPile(cell *p)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  while(p != NULL)
    {
      printf("%c", p->chiffre);
      p = p->suivant;
      tmp->suivant = p;
    }
  printf("\n");
  p = tmp;
}

//Appelle les fonctions appropriées selon l'opérateur lu
void lireOperateur(char c)
{
    switch (c) {
        case '+':
            //add();
            break;
        case '*':
            //mult
            break;
        case '-':
            //soustraction
            break;
        case '?':
            //extraire compteur de reference
            ;
    }
}

//Lecture des chiffres et creation des structs appropries
void lireChiffre(char x, int continue_nb) {
    num* n;
    cell* c;
    
    if(continue_nb == 0) //Debut d'une nouvelle operande
    {
        //Allouer un nouveau num pour l'operande
        n = (num*)malloc(sizeof(num));
        n->negatif = 0; //TODO - ajouter support pour chiffres négatif
        n->chiffres = NULL;
        
        //Ajouter un bloc a la pile d'operandes
        push_op(&pile_op, &n);
        n->compt_ptr=1;
    }
    
    //Allouer un nouveau cell pour le chiffre lu
     push(&(n->chiffres), x);
}


int main (int argc, char *argv[])
{

    cell *pile1 = NULL;
    cell *pile2 = NULL;
    cell *resAdd = NULL;
    cell *resMult = NULL;

    //push(&pile1, '1');
    push(&pile1, '7');
    push(&pile1, '5');
    printf("pile1:\n");
    printPile(pile1);
    
    push(&pile2, '4');
    //push(&pile2, '8');
    printf("pile2:\n");
    printPile(pile2);

    resAdd = add(pile1, pile2);
    printPile(resAdd);
        
    resMult = mult(pile1, pile2);
    printPile(resMult);
    
        
   /*Lecture de la ligne de commande - a completer
    char c[1];
    int continue_nb = 0; //0 = nouveau nb, 1 = continue le nb precedent
    
            
    printf(">");


    while ((c[0] = getchar()) != '\n') {
            
        if (strpbrk(c, "0123456789") != NULL) {
            printf("Trouvé entier\n");
            lireChiffre(c);
            continue_nb = 1;
        } else if (strpbrk(c, "+*-?") != NULL) {
            printf("Trouvé opérateur\n");
            lireOperateur(c);
        } else if (strpbrk(c, "=") != NULL) {
            printf("Assignation de variable\n");
        } else if (strpbrk(c, "abcdefghijklmnopqrstuvwxyz") != NULL) {
            printf("Caractère alpha\n");
        } else if (strpbrk(c, " ") != NULL) {
            continue_nb = 0; //prochain nombre commencera une nouvelle operande
        } else {
            printf("ERREUR - caractère invalide\n");
        }
    }*/

    
    return 0;
}
