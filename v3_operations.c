#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct cell
{
  char chiffre;
  struct cell *suivant;
} cell;

typedef struct num
{
  int negatif;
  struct cell *chiffres;
  int compt_ptr;
} num;

typedef struct operande
{
    struct num *nombre;
    struct operande *suivant;
} operande;


operande* pile_op = NULL; //Pile contenant les operandes lues
num* variables[26]; //Tableau de pointeurs vers des num (0 = a, 26 = z)

void printNum(num *n)
{
  cell *p = n->chiffres;
  cell *tmp = (cell *)malloc(sizeof(cell));
  if(n->negatif == 1)
    printf("-");
  while(p != NULL)
    {
      printf("%c", p->chiffre);
      p = p->suivant;
      tmp->suivant = p;
    }
  printf("\n");
  p = tmp;
  free(tmp);
}

void printPile(cell *p)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  while(p != NULL)
    {
      printf("%c", p->chiffre);
      p = p->suivant;
      tmp->suivant = p;
    }
  printf("\n");
  p = tmp;
  free(tmp);
}

//Ajoute une nouvelle cell
void push(cell **p, char a)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  tmp->chiffre = a;
  tmp->suivant = *p;
  *p = tmp;
  //printf("push %i\n", tmp->chiffre - 48);
}

//Ajouter une nouvelle cell a la fin de liste;
//comme un queue (FIFO)
struct cell *dernier;
void pushBack(cell **p, char a)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  if(*p == NULL)
    {
      tmp->chiffre = a;
      tmp->suivant = NULL;
      *p = tmp;
      dernier = tmp;
    }
  else
    {
      tmp->chiffre = a;
      tmp->suivant = NULL;
      dernier->suivant = tmp;
      dernier = tmp;
    }
  //printf("push %i\n", tmp->chiffre - 48);
}

//Ajoute une nouvelle operande 
//void push_op(operande **op, num* n)
void push_op(operande **op, num **n) 
{
    operande *tmp = (operande*)malloc(sizeof(operande));
    tmp->nombre = *n;
    tmp->suivant = *op;
    *op = tmp;
}

//C'est plus utile sachant qu'on a la fonction pushBack
/* cell * inversePile(cell *p) */
/* { */
/*   //struct cell *tmp = (struct cell *)malloc(sizeof(struct cell)); */
/*   cell *tmp = NULL; */
/*   while(p != NULL) */
/*     { */
/*       //printf("inv p %i\n", **p->chiffre - 48); */
/*       push(&tmp, p->chiffre);  */
/*       //printf("inv tmp %i\n", tmp->chiffre - 48); */
/*       p = p->suivant; */
/*     } */
/*   return tmp; */
/* } */

num *add(operande *op_pl)
{
  operande *tmp_op = (operande *)malloc(sizeof(operande));
  cell *c1 = op_pl->suivant->nombre->chiffres;
  cell *c2 = op_pl->nombre->chiffres;
  num *res = (num*)malloc(sizeof(num));

    if(c1 == NULL)
      {
    	res->negatif = op_pl->nombre->negatif;
    	res->chiffres = c2;
      }
    else if(c2 == NULL)
      {
    	res->negatif = op_pl->suivant->nombre->negatif;
    	res->chiffres = c1;
      }
    else
      {
  	do
	  {
	    //mod: modulo, r: retour, s: somme
	    int mod = 0, r = 0, s = 0;	  
	    c1 = op_pl->suivant->nombre->chiffres;
	    c2 = op_pl->nombre->chiffres;
	    while(c1 != NULL && c2 != NULL)
	      {
		//ascii code char: 0 = 48, A = 65, a = 97
		s = (c1->chiffre - 48) + (c2->chiffre - 48) + r;
		//printf("s: %i\n", s);
		mod = s % 10;
		//printf("mod: %i\n", mod);
		r = s / 10;
		//printf("r: %i\n", r);
		//push(&res, mod+48);
		pushBack(&res->chiffres, mod+48);

		c1 = c1->suivant;
		c2 = c2->suivant;
	      }
	    //Le cas le nombre de chiffre de c1 < celui de c2
	    if(c1 == NULL)
	      {
		while(c2 != NULL)
		  {
		    s = (c2->chiffre - 48) + r;
		    mod = s % 10;
		    r = s / 10;
		    //push(&res, mod+48);
		    pushBack(&res->chiffres, mod+48);
		    c2 = c2->suivant;
		  }
	      } else
	      {//Le cas le nombre de chiffre de c2 < celui de c1
		while(c1 != NULL)
		  {
		    s = (c1->chiffre - 48) + r;
		    mod = s % 10;
		    r = s / 10;
		    //push(&res, mod+48);
		    pushBack(&res->chiffres, mod+48);
		    c1 = c1->suivant;
		  }
	      }
	    if(r != 0)
	      push(&res->chiffres, r+48);
	    op_pl->suivant->nombre = res;
	    op_pl = op_pl->suivant;
	    tmp_op->suivant = op_pl;
	    printNum(op_pl->nombre);
	    printNum(op_pl->suivant->nombre);
	    //printNum(op_pl->suivant->nombre);
	  }while(op_pl->suivant !=NULL);
	}

  return res;
}

num *soust(operande *op_pl)
{
  cell *c1 = op_pl->suivant->nombre->chiffres;
  cell *c2 = op_pl->nombre->chiffres;
  num *res = res = (num*)malloc(sizeof(num));

  if(c1 == NULL)
    {
      res->negatif = 1;
      res->chiffres = c2;
    }
  else if(c2 == NULL)
    {
      res->negatif = op_pl->suivant->nombre->negatif;
      res->chiffres = c1;
    }
  else
    {
      //resulatat = som += (c1 - c2)*10^compt
      //temp contient le resultat de soustraction de chq
      //chiffre des liste c1 et c2
      int som = 0, compt = 0, temp = 0;
      while(c1 != NULL && c2 != NULL)
	{
	  //ascii code char: 0 = 48, A = 65, a = 97
	  temp = ((c1->chiffre - 48) - (c2->chiffre - 48));
	  for(int i = 0; i < compt; ++i)
	    {
	      temp *= 10;
	    }
	  som += temp;
	  ++compt;
	  c1 = c1->suivant;
	  c2 = c2->suivant;
	}
      if(c1 == NULL)
	{
	  while(c2 != NULL)
	    {
	      temp = (0 - (c2->chiffre - 48));
	      for(int i = 0; i < compt; ++i)
		{
		  temp *= 10;
		}
	      som += temp;
	      ++compt;
	      c2 = c2->suivant;
	    }
	}else
	{
	  while(c1 != NULL)
	    {
	      temp = c1->chiffre - 48;
	      for(int i = 0; i < compt; ++i)
		{
		  temp *= 10;
		}
	      som += temp;
	      ++compt;
	      c1 = c1->suivant;
	    }
	}

      //printf("som: %i\n", som);
      //On met le resultat de soustraction
      //dans une pile des cells
      int mod = 0;
      //cell *resCell = NULL;

      //res->chiffres = resCell;
      res->compt_ptr = 1;
      if(som < 0)
	{
	  res->negatif = 1;
	  //pour le rendre en valeur absolue
	  som = -som;
	}
      else if (som > 0)
	//??? peut-etre qu'on peut mettre 0 pour
	//??? le signe comme la valeur par defaut
	res->negatif = 0;
      {
	do
	  {
	    mod = som % 10;
	    pushBack(&res->chiffres, mod+48);
	    som = som / 10;
	  }
	while(som > 0);
      }
    }
    return res;
}

num *mult(operande *op_pl)
{
  operande *tmp_op = NULL;
  cell *c1 = op_pl->suivant->nombre->chiffres;
  cell *tmp_c1 = c1;
  cell *c2 = op_pl->nombre->chiffres;
  
  //cell *res = NULL;
  num *res = (num *)malloc(sizeof(num));

  if(c1 == NULL || c2 == NULL)
    {
      return res;
    }
else
    {
      //pour mettre les zeros chq fois qu'on lit un nouveau chiffre
      //de c2 qui est 10 fois plus que celui avant
      int compt = 0;
      while(c2 != NULL)  
	{
	  //mul:  multiplication, r: retour, mod: modulo
	  int mul = 0, r = 0 , mod = 0;
	  num *n = (num*)malloc(sizeof(num));
	  n->negatif = 0; 
	  n->chiffres = NULL;
	  push_op(&tmp_op, &n);	  
	  for(int i = 0; i < compt; ++i)
	    pushBack(&n->chiffres, 48);
	  while(tmp_c1 != NULL)
	    {
	      mul = (c2->chiffre - 48) * (tmp_c1->chiffre - 48) + r;
	      mod = mul % 10;
	      r = mul / 10;
	      pushBack(&n->chiffres, mod+48);
	      //printf("mul %i\n", mod);
	      printf("%i\n", tmp_c1->chiffre-48);
	      tmp_c1 = tmp_c1->suivant;
	    }
	  if (r != 0)
	    pushBack(&n->chiffres, r+48);
	  tmp_c1 = c1;
	  //printf("%i\n", c2->chiffre-48);
	  c2 = c2->suivant;
	  ++compt;
	  //printPile(tmp_op->nombre->chiffres);
	}
    }
  //printNum(tmp_op->nombre);
  //printNum(tmp_op->suivant->nombre);
  //res = add(tmp_op);
  return res;
}

//Appelle les fonctions appropriées selon l'opérateur lu
void lireOperateur(char c)
{
    switch (c) {
        case '+':
            //add();
            break;
        case '*':
            //mult
            break;
        case '-':
            //soustraction
            break;
        case '?':
            //extraire compteur de reference
            ;
    }
}

//Lecture des chiffres et creation des structs appropries
void lireChiffre(char x, int continue_nb) {
    num* n;
    cell* c;
    
    if(continue_nb == 0) //Debut d'une nouvelle operande
    {
        //Allouer un nouveau num pour l'operande
        n = (num*)malloc(sizeof(num));
        n->negatif = 0; //TODO - ajouter support pour chiffres négatif
        n->chiffres = NULL;
        
        //Ajouter un bloc a la pile d'operandes
        push_op(&pile_op, &n);
        n->compt_ptr=1;
    }
    
    //Allouer un nouveau cell pour le chiffre lu
    push(&n->chiffres, x);
}


int main (int argc, char *argv[])
{

  //cell *pile_cell_1 = NULL;
  //cell *pile_cell_2 = NULL;
    num *resAdd = NULL;
    num *resMult = NULL;
    num *resSoust = NULL;

    lireChiffre('1', 0);
    //lireChiffre('5', 1);

    lireChiffre('2', 0);
    lireChiffre('4', 1);
    lireChiffre('6', 1);
    printPile(pile_op->suivant->nombre->chiffres);
    printPile(pile_op->nombre->chiffres);
    
    printf("add:\n");
    resAdd = add(pile_op);
    //printPile(resAdd);
    printNum(resAdd);
            
    printf("mult:\n");
    resMult = mult(pile_op);
    //printPile(resMult);
    printNum(resMult);

    printf("sout:\n");
    resSoust = soust(pile_op);
    //printPile(resSoust->chiffres);
    printNum(resSoust);


   /*Lecture de la ligne de commande - a completer
    char c[1];
    int continue_nb = 0; //0 = nouveau nb, 1 = continue le nb precedent
    
            
    printf(">");


    while ((c[0] = getchar()) != '\n') {
            
        if (strpbrk(c, "0123456789") != NULL) {
            printf("Trouvé entier\n");
            lireChiffre(c);
            continue_nb = 1;
        } else if (strpbrk(c, "+*-?") != NULL) {
            printf("Trouvé opérateur\n");
            lireOperateur(c);
        } else if (strpbrk(c, "=") != NULL) {
            printf("Assignation de variable\n");
        } else if (strpbrk(c, "abcdefghijklmnopqrstuvwxyz") != NULL) {
            printf("Caractère alpha\n");
        } else if (strpbrk(c, " ") != NULL) {
            continue_nb = 0; //prochain nombre commencera une nouvelle operande
        } else {
            printf("ERREUR - caractère invalide\n");
        }
    }*/

    
    return 0;
}
