#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct cell
{
  char chiffre;
  struct cell *suivant;
};

struct num
{
  int negatif;
  struct cell *chiffres;
  int compt_ptr;
};

void push(struct cell **p, char a)
{
  struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  tmp->chiffre = a;
  tmp->suivant = *p;
  *p = tmp;
  //printf("push %i\n", tmp->chiffre - 48);
}

struct cell * inversePile(struct cell *p)
{
  //struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  struct cell *tmp = NULL;
  while(p != NULL)
    {
      //printf("inv p %i\n", **p->chiffre - 48);
      push(&tmp, p->chiffre); 
      //printf("inv tmp %i\n", tmp->chiffre - 48);
      p = p->suivant;
    }
  return tmp;
}

struct cell *add(struct cell *p1, struct cell *p2)
{
  //mod: modulo, r: retour, s: somme
  int mod = 0, r = 0, s = 0;
  //struct cell *res = (struct cell *)malloc(sizeof(struct cell));
  struct cell *res = NULL;
  while(p1 != NULL && p2 != NULL)
    {
      //ascii code char: 0 = 48, A = 65, a = 97
      s = (p1->chiffre - 48) + (p2->chiffre - 48) + r;
      //printf("s: %i\n", s);
      mod = s % 10;
      //printf("mod: %i\n", mod);
      r = s / 10;
      //printf("r: %i\n", r);
      //push(&res, mod+48);
      push(&res, mod+48);

      p1 = p1->suivant;
      p2 = p2->suivant;
    }
  
  if(p1 == NULL)
    {
      while(p2 != NULL)
	{
	  s = (p2->chiffre - 48) + r;
	  mod = s % 10;
	  r = s / 10;
	  //push(&res, mod+48);
	  push(&res, mod+48);
	  p2 = p2->suivant;
	}
    } else
    {
      while(p1 != NULL)
	{
	  s = (p1->chiffre - 48) + r;
	  mod = s % 10;
	  r = s / 10;
	  //push(&res, mod+48);
	  push(&res, mod+48);
	  p1 = p1->suivant;
	}
    }
  if(r != 0)
    push(&res, r+48);
  return  inversePile(res);
}

struct cell *mult(struct cell *p1, struct cell *p2)
{
  //mul:  multiplication, r: retour, mod: modulo
  int mul = 0, r = 0 , mod = 0;
  struct cell * tmp1 = NULL;
  struct cell * tmp2 = NULL;
  //struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  struct cell *tmp = NULL;
 
  while(p2 != NULL)  
    {
      while(p1 != NULL)
	{
	  mul = (p2->chiffre - 48) * (p1->chiffre - 48) + r;
	  mod = mul % 10;
	  r = mul / 10;
	  //printf("mult mul %i\n", mul);
	  //printf("mult mod %i\n", mod);
	  //printf("mult r %i\n", r);
	  push(&tmp, mod+48);
	  //printf("mult tmp %i\n", tmp->chiffre - 48);
	  p1 = p1->suivant;
	}
      if (r != 0)
	push(&tmp, r+48);
      //tmp1 = 
      p2 = p2->suivant;
    }
  return inversePile(tmp);
}

void printPile(struct cell *p)
{
  struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  while(p != NULL)
    {
      printf("%c", p->chiffre);
      p = p->suivant;
      tmp->suivant = p;
    }
  printf("\n");
  p = tmp;
}


int main (int argc, char *argv[])
{

  struct cell *pile1 = NULL;
  struct cell *pile2 = NULL;
  struct cell *resAdd = NULL;
  struct cell *resMult = NULL;

  //push(&pile1, '1');
  push(&pile1, '7');
  push(&pile1, '5');
  printf("pile1:\n");
  printPile(pile1);
  
  push(&pile2, '4');
  //push(&pile2, '8');
  printf("pile2:\n");
  printPile(pile2);

  resAdd = add(pile1, pile2);
  printPile(resAdd);
    
  resMult = mult(pile1, pile2);
  printPile(resMult);
  
  return 0;
}
