#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct cell
{
  char chiffre;
  struct cell *suivant;
} cell;

typedef struct num
{
  int negatif;
  struct cell *chiffres;
  int compt_ptr;
} num;

typedef struct operande
{
    struct num *nombre;
    struct operande *suivant;
} operande;


operande* pile_op = NULL; //Pile contenant les operandes lus
num* variables[26]; //Tableau de pointeurs vers des num (0 = a, 26 = z)

void suppNum(num *ptr){
    
    while (ptr->chiffres != NULL) //Supprimer toutes les cells
    {
        cell* cell_tmp = ptr->chiffres;
        ptr->chiffres = ptr->chiffres->suivant;
        free(cell_tmp);
    }
    
    free(ptr); //Supprimer le num
}

//Vide la pile d'opérandes
void videPileOp()
{
    
    while (pile_op != NULL)
    {
        
        
        if (--(pile_op->nombre->compt_ptr) == 0) //Si aucune autre référence au num, supprimer
        {
           suppNum(pile_op->nombre);
        }
        
        operande *op_tmp = pile_op;
        pile_op = pile_op->suivant;
        free(op_tmp);
    
        
    }
}

//Imprime le compteur de référence d'un nombre
void printCompteur() 
{
    if (pile_op == NULL)
    {
        printf("Erreur - opérande manquant (usage: <opérande> ?)\n");
    } else {
        printf("%d \n", pile_op->nombre->compt_ptr-1); //Enlever 1 car on ne compte pas le lien avec le bloc d'opérandes
    }
}

//Ajoute une nouvelle cell
void push(cell **p, char a)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  tmp->chiffre = a;
  tmp->suivant = *p;
  *p = tmp;
  //printf("push %i\n", tmp->chiffre - 48);
}

//Ajouter une nouvelle cell a la fin de liste;
//comme un queue (FIFO)
struct cell *dernier;
void pushBack(cell **p, char a)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  if(*p == NULL)
    {
      tmp->chiffre = a;
      tmp->suivant = NULL;
      *p = tmp;
      dernier = tmp;
    }
  else
    {
      tmp->chiffre = a;
      tmp->suivant = NULL;
      dernier->suivant = tmp;
      dernier = tmp;
    }
  //printf("push %i\n", tmp->chiffre - 48);
}

//Ajoute un nouvel operande 
//void push_op(operande **op, num* n)
void push_op(operande **op, num **n) 
{
    operande *tmp = (operande*)malloc(sizeof(operande));
    tmp->nombre = *n;
    tmp->suivant = *op;
    *op = tmp;
}

//C'est plus utile sachant qu'on a la fonction pushBack
cell * inversePile(cell *p)
{
  //struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  cell *tmp = NULL;
  while(p != NULL)
    {
      //printf("inv p %i\n", **p->chiffre - 48);
      push(&tmp, p->chiffre); 
      //printf("inv tmp %i\n", tmp->chiffre - 48);
      p = p->suivant;
    }
  return tmp;
}

/*cell *add(cell *p1, cell *p2)
{
  //mod: modulo, r: retour, s: somme
  int mod = 0, r = 0, s = 0;
  //struct cell *res = (struct cell *)malloc(sizeof(struct cell));
  cell *res = NULL;
  while(p1 != NULL && p2 != NULL)
    {
      //ascii code char: 0 = 48, A = 65, a = 97
      s = (p1->chiffre - 48) + (p2->chiffre - 48) + r;
      //printf("s: %i\n", s);
      mod = s % 10;
      //printf("mod: %i\n", mod);
      r = s / 10;
      //printf("r: %i\n", r);
      //push(&res, mod+48);
      pushBack(&res, mod+48);

      p1 = p1->suivant;
      p2 = p2->suivant;
    }
  //Le cas le nombre de chiffre de p1 < celui de p2
  if(p1 == NULL)
    {
      while(p2 != NULL)
	{
	  s = (p2->chiffre - 48) + r;
	  mod = s % 10;
	  r = s / 10;
	  //push(&res, mod+48);
	  pushBack(&res, mod+48);
	  p2 = p2->suivant;
	}
    } else
    {//Le cas le nombre de chiffre de p2 < celui de p1
      while(p1 != NULL)
	{
	  s = (p1->chiffre - 48) + r;
	  mod = s % 10;
	  r = s / 10;
	  //push(&res, mod+48);
	  pushBack(&res, mod+48);
	  p1 = p1->suivant;
	}
    }
  if(r != 0)
    push(&res, r+48);
  return  res;
}*/

void add(cell *p1, cell*p2) {
      printf("ADDITION APPELÉE\n");
}

/*cell *mult(cell *p1, cell *p2)
{
  //mul:  multiplication, r: retour, mod: modulo
  int mul = 0, r = 0 , mod = 0;
  cell * tmp1 = NULL;
  cell * tmp2 = NULL;
  //struct cell *tmp = (struct cell *)malloc(sizeof(struct cell));
  cell *tmp = NULL;
 
  while(p2 != NULL)  
    {
      while(p1 != NULL)
	{
	  mul = (p2->chiffre - 48) * (p1->chiffre - 48) + r;
	  mod = mul % 10;
	  r = mul / 10;
	  //printf("mult mul %i\n", mul);
	  //printf("mult mod %i\n", mod);
	  //printf("mult r %i\n", r);
	  push(&tmp, mod+48);
	  //printf("mult tmp %i\n", tmp->chiffre - 48);
	  p1 = p1->suivant;
	}
      if (r != 0)
	push(&tmp, r+48);
      //tmp1 = 
      p2 = p2->suivant;
    }
  return inversePile(tmp);
}*/

void mult(cell *p1, cell*p2) {
    printf("MULTIPLICATION APPELÉE\n");
}

void sub(cell *p1, cell*p2) {
    printf("SOUSTRACTION APPELÉE\n");
}

void printPile(cell *p)
{
  cell *tmp = (cell *)malloc(sizeof(cell));
  while(p != NULL)
    {
      printf("%c", p->chiffre);
      p = p->suivant;
      tmp->suivant = p;
    }
  printf("\n");
  p = tmp;
}

//FONCTION TEST A SUPPRIMER
void printPileOp(){
    operande *op_tmp = pile_op;
    cell *print_pile = NULL;
    cell *print_ptr = NULL;
    
    if (pile_op == NULL) {
        printf("PILE VIDE\n");
    } else {
    
    int i = 0;
    printf("Top de la pile\n");

    while (op_tmp != NULL){
        printf("operande %d : ", i);
        if (op_tmp->nombre->negatif == 1) {
            printf("-");
        }
        cell *cell_tmp = op_tmp->nombre->chiffres;
        while (cell_tmp != NULL) {
            push(&print_pile, cell_tmp->chiffre);
            cell_tmp = cell_tmp->suivant;
        }
        print_ptr = print_pile;
        while(print_pile != NULL) {
            printf("%d", print_ptr->chiffre-48);
            print_ptr = print_ptr->suivant;
            free(print_pile);
            print_pile = print_ptr;
        }
        printf("\n");
        op_tmp = op_tmp->suivant;
        i++;
    }
    printf("Bas de la pile\n");
    }
}

//FONCTION TEST A SUPPRIMER
void printVariables(){
    int i;
    for (i = 0; i < 26; i++) {
        printf("%c :", (char)i+97);
        
       if (variables[i] != NULL) 
        {
            cell *print_pile = NULL;
            cell *print_ptr = NULL;
            
            cell *cell_tmp = variables[i]->chiffres;
            while (cell_tmp != NULL) {
                push(&print_pile, cell_tmp->chiffre);
                cell_tmp = cell_tmp->suivant;
            }
            print_ptr = print_pile;
            while(print_pile != NULL) {
                printf("%d", print_ptr->chiffre-48);
                print_ptr = print_ptr->suivant;
                free(print_pile);
                print_pile = print_ptr;
            }  
        } else
        {
            printf("NULL");
        }
        
        printf("  ");
    }
    printf("\n");
}

//Appelle les fonctions appropriées selon l'opérateur lu
void lireOperateur(char c)
{
    switch (c) {
        case '+':
            add(pile_op->suivant->nombre->chiffres, pile_op->nombre->chiffres);
            break;
        case '*':
            mult(pile_op->suivant->nombre->chiffres, pile_op->nombre->chiffres);
            break;
        case '-':
            sub(pile_op->suivant->nombre->chiffres, pile_op->nombre->chiffres);
            break;
        case '?':
            printCompteur();
            ;
    }
}

//Lecture des chiffres et creation des structs appropries
void lireChiffre(char x, int continue_nb) 
{
    num* n;
    //cell* c;
    
    if(!continue_nb) //Debut d'un nouvel operande
    {
        //Allouer un nouveau num pour l'operande
        n = (num*)malloc(sizeof(num));
        
        if (n == NULL) 
        {
            printf("ERREUR - Mémoire insuffisante\n");
            exit(EXIT_FAILURE);
        }
        
        n->negatif = 0; //TODO - ajouter support pour chiffres négatif
        n->chiffres = NULL;
        //printf("NOUVEAU NUM ALLOUÉ\n");
        
        //Ajouter un bloc a la pile d'operandes
        push_op(&pile_op, &n);
        n->compt_ptr = 1;
        //printf("NOUVEL OPERANDE ALLOUÉ\n");
    }
    
    //Allouer un nouveau cell pour le chiffre lu
    push(&pile_op->nombre->chiffres, x);
    //printf("NOUVEAU CELL ALLOUÉ\n");
}

/* Vérifie si le caractère est valide pour une variable
 * Retourne l'index dans le tableau des variables si valide, -1 sinon
*/
int validerVariable(char var) 
{
    int index = (int)var-97;
    if (index < 0 || index > 25) 
    {
        printf("Erreur - caractère invalide - variable doit être une lettre minuscule de a à z(\n");
        return -1;
    }
    
    return index;
}

//Place la variable appelée dans la pile des opérandes
void lireVariable(char var)
{
    int index = validerVariable(var);
    
    if (index != -1) 
    {
        if (variables[index] == NULL) {
            printf("ERREUR - Variable %c non initialisée. Pour assigner une valeur : <expression> =<variable>", var);
        } else {
            //Ajouter un bloc a la pile d'operandes
            push_op(&pile_op, &variables[index]);
            variables[index]->compt_ptr++;
        }

    }
}

//Assigne le dernier opérande lu à une variable
void assignVariable(char var) 
{
    printf("ASSIGNATION À lA VARIABLE %c\n", var);
    if (pile_op == NULL) 
    {
        printf("ERREUR - aucun opérande à assigner\n");
    } else 
    {
        int index = validerVariable(var);
        if (index != -1) 
        {
            if (variables[index] != NULL) //Variable déjà occupée
            {   
                
                if (--(variables[index]->compt_ptr) == 0) { //Supprimer le num si compteur de référence à 0
                    printf("SUPPRESSION ANCIENNE VARIABLE %c", (char)index+97);
                    suppNum(variables[index]);
                }
            }
            
        variables[index] = pile_op->nombre;
        pile_op->nombre->compt_ptr++;
        
        }
    }
}



int detectEntier(char c) {
    return (c >= 48 && c <=57);
}

int detectAlpha(char c){
    return (c >= 97 && c <= 122);
}

int detectOp(char c) {
    return (c == 42 || c == 43 || c == 45 || c == 63);
}

int main (int argc, char *argv[])
{

    cell *pile_cell_1 = NULL;
    cell *pile_cell_2 = NULL;
    cell *resAdd = NULL;
    cell *resMult = NULL;

    /* push(&pile_cell_1, '1'); */
    /* push(&pile_cell_1, '7'); */
    /* push(&pile_cell_1, '5'); */

    /* pushBack(&pile_cell_1, '1'); */
    /* pushBack(&pile_cell_1, '7'); */
    /* pushBack(&pile_cell_1, '5'); */
    
    /*lireChiffre('1', 0);
    lireChiffre('7', 1);
    printPile(pile_op->nombre->chiffres);*/

    /*TESTS VARIABLES
    lireChiffre('4', 0);
    lireChiffre('8', 1);
    printPile(pile_op->nombre->chiffres);
    
    lireChiffre('1', 0);
    lireChiffre('7', 1);
    printPile(pile_op->nombre->chiffres);
    
     
    assignVariable('x');
    assignVariable('w');
    
    printCompteur();
    
    printVariables();*/
   

    //printf("pile_cell_1:\n");
    //printPile(pile_cell_1);
    
        
    /* push(&pile_cell_2, '4'); */
    /* push(&pile_cell_2, '8'); */
    /* printf("pile_cell_2:\n"); */
    /* printPile(pile_cell_2); */

    //printf("pile_cell_1 + pile_cell_2:\n");
    //resAdd = add(pile_op->suivant->nombre->chiffres, pile_op->nombre->chiffres);
    //printPile(resAdd);
        
    /* printf("mult:\n"); */
    /* resMult = mult(pile_cell_1, pile_cell_2); */
    /* printPile(resMult) */;
    
        
   //Lecture de la ligne de commande - a completer

    
    while (!feof(stdin)) { //Tant qu'on n'obtient pas EOF (ctrl+D)
            
        char c;
        int continue_nb = 0; //0 = nouveau nb, 1 = continue le nb precedent
        
        printf("Contenu de pile_op avant opérations: \n");
        printPileOp();
        printVariables();
        
        printf(">");
        
        while ((c = getchar()) != '\n' && c != EOF) {
            
            if (detectEntier(c)) // entier
            {
                printf("Trouvé entier : %d\n", c-48);
                lireChiffre(c, continue_nb);
                continue_nb = 1;
                
             } 
            else if (detectOp(c)) // +*-?
            {
                printf("Trouvé opérateur : %c\n", c);
                lireOperateur(c);
            } 
            else if (c == 61)       // =
            {
                printf("Assignation de variable\n");
                if (detectAlpha(c = getchar())) {
                    assignVariable(c);
                } else {
                    printf("ERREUR - Variable invalide - Usage: =<variable>\n");
                }
            } 
            else if (detectAlpha(c)) //caractère alpha
            {
                printf("Caractère alpha\n");
                lireVariable(c);
                
            } 
            else if (c == 32) // espace
            {
                continue_nb = 0; //prochain nombre commencera un nouvel operande
                
            } else {
                printf("ERREUR - caractère invalide\n");
            }
        }
        
        printf("Contenu de pile_op après opérations: \n");
        printPileOp();
        printVariables();
        
        //Vider le contenu de pile_op
        videPileOp(); //A TESTER
            
    }
    
    printf("\nProgramme terminé\n");
    return 0;
}
